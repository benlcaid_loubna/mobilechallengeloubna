package com.cires.mobilechallenge.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cires.mobilechallenge.Login.LoginFragment;
import com.cires.mobilechallenge.R;
import com.ramotion.paperonboarding.PaperOnboardingFragment;
import com.ramotion.paperonboarding.PaperOnboardingPage;

import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    Button Bt_skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide();
        fragmentManager = getSupportFragmentManager();
        Bt_skip = findViewById(R.id.Bt_skip);
         if (!readFromSHaredPref()) {
            saveInSharedPref();
            OnBoarDing();
            SkipOnBoarding();
        }
        else
            OpenLoginUi();
    }
    void saveInSharedPref() {
        SharedPreferences sharedPref = getSharedPreferences("mobileChallenge", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("FIRSTLOGIN", true);
        editor.apply();
    }
    boolean readFromSHaredPref() {
        SharedPreferences sharedPref = getSharedPreferences("mobileChallenge", Context.MODE_PRIVATE);
        return sharedPref.getBoolean("FIRSTLOGIN", false);
    }

    private void OpenLoginUi() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment bf = new LoginFragment();
        fragmentTransaction.replace(R.id.frame_layout, bf);
        fragmentTransaction.commit();
        Bt_skip.setVisibility(View.GONE);

    }

    private void SkipOnBoarding() {
        Bt_skip.setVisibility(View.VISIBLE);
        Bt_skip.setOnClickListener(view -> {
            OpenLoginUi();
        });
    }

    private void OnBoarDing() {
        final PaperOnboardingFragment paperOnboardingFragment = PaperOnboardingFragment.newInstance(GetDataforOnboarding());
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.frame_layout, paperOnboardingFragment);
        paperOnboardingFragment.setOnRightOutListener(this::OpenLoginUi);
        fragmentTransaction.commit();
    }

    private ArrayList<PaperOnboardingPage> GetDataforOnboarding() {
        PaperOnboardingPage source = new PaperOnboardingPage(getString(R.string.title1), getString(R.string.description1), Color.parseColor("#ffffff"), R.drawable.onboard_page1, com.ramotion.paperonboarding.R.drawable.onboarding_pager_circle_icon);
        PaperOnboardingPage source1 = new PaperOnboardingPage(getString(R.string.title2), getString(R.string.description2), Color.parseColor("#c9c9c9"), R.drawable.onboard_page2, com.ramotion.paperonboarding.R.drawable.onboarding_pager_circle_icon);
        PaperOnboardingPage source2 = new PaperOnboardingPage(getString(R.string.title3), getString(R.string.description3), Color.parseColor("#64def9"), R.drawable.onboard_page3, com.ramotion.paperonboarding.R.drawable.onboarding_pager_circle_icon);
        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(source);
        elements.add(source1);
        elements.add(source2);
        return elements;
    }
}