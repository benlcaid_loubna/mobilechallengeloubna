package com.cires.mobilechallenge.Activities;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.cires.mobilechallenge.Adapters.ViewPagerFragmentAdapter;
import com.cires.mobilechallenge.R;
 import com.cires.mobilechallenge.databinding.ActivityScrollingNewsBinding;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.Arrays;
import java.util.List;

public class NewsScrollingActivity extends AppCompatActivity {

    ViewPager2 viewPager2;
    ViewPagerFragmentAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityScrollingNewsBinding binding = ActivityScrollingNewsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        List<String> lstCategories =
                Arrays.asList("Science", "Business", "Sports", "Technology", "Stratup", "Automobile");


        Toolbar toolbar = binding.toolbar;
        TabLayout tabLayout = binding.tabs;
        viewPager2 = findViewById(R.id.view_pager);
        setSupportActionBar(toolbar);
        myAdapter = new ViewPagerFragmentAdapter(getSupportFragmentManager(), getLifecycle());
        // set Orientation  in ViewPager2
        viewPager2.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewPager2.setAdapter(myAdapter);
        viewPager2.setPageTransformer(new MarginPageTransformer(1500));
        new TabLayoutMediator(tabLayout, viewPager2,
                (tab, position) -> {
                    try {
                        tab.setText(lstCategories.get(position));
                    } catch (Exception e) {
                        Log.d("tester", " position ex :" + e);
                    }
                }).attach();
    }
}