package com.cires.mobilechallenge.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cires.mobilechallenge.Api.Models.NEWS;
import com.cires.mobilechallenge.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.ViewHolder> {
    private final ArrayList<NEWS.DATA> lstNews;
    Context mContext;
    public AdapterRecyclerView(ArrayList<NEWS.DATA> news, Context context) {
        mContext = context;
        lstNews = news;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.list_items, parent, false);
        result = contactView;

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    View result;

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        // Get the data model using position
        NEWS.DATA news = lstNews.get(position);
        // Set item views based on your views and data model
        holder.recy_title.setText(news.title);
        holder.recy_date.setText(news.time);
        loadImage(news.imageUrl, holder.recy_imageView);
        holder.recy_author.setText(news.author);
        holder.itemView.setOnClickListener(view -> {
        });

        result = holder.recy_title.getRootView();
    }


    @Override
    public int getItemCount() {
        if (lstNews != null) return lstNews.size();
        else return 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView recy_title;
        public ImageView recy_imageView;
        public TextView recy_author;
        public TextView recy_date;

        public ViewHolder(View itemView) {
            super(itemView);
            recy_title = itemView.findViewById(R.id.news_title);
            recy_imageView = itemView.findViewById(R.id.news_image);
            recy_author = itemView.findViewById(R.id.news_author);
            recy_date = itemView.findViewById(R.id.news_date);
        }
    }

    private void loadImage(String url, ImageView imageview) {
        Picasso.get()
                .load(url)
                .into(imageview);
    }

}

