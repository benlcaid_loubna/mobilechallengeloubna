package com.cires.mobilechallenge.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import com.cires.mobilechallenge.Fragments.NewsFragment;

public class ViewPagerFragmentAdapter extends FragmentStateAdapter {

    public ViewPagerFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new NewsFragment("science");
            case 1:
                return new NewsFragment("business");
            case 2:
                return new NewsFragment("sports");
            case 3:
                return new NewsFragment("technology");
            case 4:
                return new NewsFragment("startup");
            case 5:
                return new NewsFragment("automobile");

        }
        return null;
    }

    @Override
    public int getItemCount() {
        return 6;
    }
}