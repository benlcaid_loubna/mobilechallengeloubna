package com.cires.mobilechallenge.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.cires.mobilechallenge.Adapters.AdapterRecyclerView;
import com.cires.mobilechallenge.Api.Models.NEWS;
import com.cires.mobilechallenge.Api.Services.NewsService;
import com.cires.mobilechallenge.Methodes;
import com.cires.mobilechallenge.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewsFragment extends Fragment {
    View view;
    Methodes methodes;
    Retrofit mRetrofit;
    RecyclerView recyclerview;
    AdapterRecyclerView mAdapter;
    String mCategory;
    private ProgressBar spinner;
    private LinearLayout ln_noData;
    private TextView tv_noData;
    ArrayList<NEWS.DATA> newsData;

    public NewsFragment(String category) {
        mCategory = category;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    SwipeRefreshLayout swipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_news, container, false);
        //FragmentNewsBinding binding = FragmentNewsBinding.inflate(getLayoutInflater());
        //instance
        recyclerview = view.findViewById(R.id.m_recyclerview);
        spinner = view.findViewById(R.id.progressBar);
        ln_noData = view.findViewById(R.id.ln_nodata);
        tv_noData = view.findViewById(R.id.tv_noData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerview.setLayoutManager(layoutManager);
        // define an adapter
        mAdapter = new AdapterRecyclerView(null, getContext());
        recyclerview.setAdapter(mAdapter);
        methodes = new Methodes();
        mRetrofit = methodes.getClassServiceRetrofit();
        spinner.setVisibility(View.VISIBLE);
        LoadData();
        return view;
    }

    private void LoadData() {
        if (methodes.isNetworkConnected(getContext()) && mRetrofit != null) {
            GetNewsScience();
        } else {
            methodes.VisibilityViews(newsData, ln_noData, recyclerview, spinner);
            Log.d("tester", " NO CNX");
        }
    }

    void AddNewsToList() {


        AdapterRecyclerView adapter = new AdapterRecyclerView(newsData, getContext());    // Create adapter passing in the sample user data
        recyclerview.addItemDecoration(new DividerItemDecoration(
                getContext(), DividerItemDecoration.VERTICAL));
        recyclerview.setAdapter(adapter);    // Attach the adapter to the recyclerview
        recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));    // Set layout manager to position the items
        methodes.VisibilityViews(newsData, ln_noData, recyclerview, spinner);

    }
    private void GetNewsScience() {
        Log.d("tester", "mcat :" + mCategory);
        NewsService newsService = mRetrofit.create(NewsService.class);
        Call<NEWS> newsCall = newsService.GetNewsByCategory(mCategory );
        newsCall.clone().enqueue(new Callback<NEWS>() {
            @Override
            public void onResponse(@NonNull Call<NEWS> call, @NonNull Response<NEWS> response) {
                Log.d("tester", " response :" + response);
                try {
                    Log.d("tester", " response er:" + response.errorBody().string());
                } catch (Exception ignored){}

                if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
                    newsData = response.body().data;
                    Log.d("tester", " newsdata :" + newsData.size());
                    if (newsData != null && newsData.size() != 0) {
                        AddNewsToList();
                    } else {
                        methodes.VisibilityViews(newsData, ln_noData, recyclerview, spinner);
                        tv_noData.setText(R.string.no_data);
                    }
                } else {
                    methodes.VisibilityViews(newsData, ln_noData, recyclerview, spinner);
                    tv_noData.setText(R.string.no_data);
                }
            }

            @Override
            public void onFailure(@NonNull Call<NEWS> call, @NonNull Throwable t) {
                methodes.VisibilityViews(newsData, ln_noData, recyclerview, spinner);
                tv_noData.setText(R.string.error_srv);
            }

        });
    }


}