package com.cires.mobilechallenge.Login;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cires.mobilechallenge.R;
import com.cires.mobilechallenge.Activities.NewsScrollingActivity;


public class LoginFragment extends Fragment {


    View view;
    private EditText Et_password;
    private EditText Et_username;
    private TextView Tv_invalidUser;
    private Button Bt_Login;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void Login() {
        //Authentification valide avec l'utilisateur `muser/mpassw0rd`
        //Afficher le message "Ce compte est bloqué" pour l'utilisateur `muser02/mpassword`
        // Pour tout autre login, afficher un message d'erreur

        Editable mPassword = Et_password.getText();
        Editable mUsername = Et_username.getText();
        Boolean IsUserNameEmpty = mUsername.toString().matches("");
        Boolean IsPasswordEmpty = mPassword.toString().matches("");
        if (IsUserNameEmpty || IsPasswordEmpty) {
             Toast.makeText(getContext(), "Username or password is empty !", Toast.LENGTH_SHORT).show();

        } else if(mUsername.toString().matches("muser") && mPassword.toString().matches("mpassw0rd")){
            {
                 Toast.makeText(getContext(), "Connected", Toast.LENGTH_SHORT).show();
                Tv_invalidUser.setVisibility(View.GONE);
                Intent intent = new Intent(getContext(), NewsScrollingActivity.class);
                 startActivity(intent);
            }

        }else if(mUsername.toString().matches("muser02") && mPassword.toString().matches("mpassword")){
            Tv_invalidUser.setText(R.string.blockedUser);
            Tv_invalidUser.setVisibility(View.VISIBLE);
        }
        else
        {
            Tv_invalidUser.setVisibility(View.VISIBLE);
            Tv_invalidUser.setText(R.string.invalidUser);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_login, container, false);
        Et_password = view.findViewById(R.id.Et_password);
        Et_username = view.findViewById(R.id.Et_username);
        Tv_invalidUser = view.findViewById(R.id.invalidUser);
        Bt_Login = view.findViewById(R.id.Bt_Login);
        Bt_Login.setOnClickListener(view -> Login());
         return view;
    }
}