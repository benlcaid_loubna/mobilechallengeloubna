package com.cires.mobilechallenge.Api.Services;


import com.cires.mobilechallenge.Api.Models.NEWS;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface NewsService {
     @GET("news")
     Call<NEWS> GetNewsByCategory(@Query("category") String category);
}
