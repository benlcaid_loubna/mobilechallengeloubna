package com.cires.mobilechallenge.Api.Models;

import java.util.ArrayList;

public class NEWS {
    public String category;
    public ArrayList<DATA> data;
    public boolean success;



    public class DATA{
        public String author;
        public String content;
        public String date;
        public String id;
        public String imageUrl;
        public String readMoreUrl;
        public String time;
        public String title;
        public String url;
    }

}
