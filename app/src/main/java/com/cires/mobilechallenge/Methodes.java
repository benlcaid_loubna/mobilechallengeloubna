package com.cires.mobilechallenge;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.cires.mobilechallenge.Api.Models.NEWS;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Methodes extends AppCompatActivity {
    /******* RETROFIT *******/
    public Retrofit getClassServiceRetrofit() {
        Retrofit retrofit = null;
        try {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            okhttp3.OkHttpClient okHttpClient = new okhttp3.OkHttpClient.Builder()
                     .connectTimeout(5, java.util.concurrent.TimeUnit.MINUTES)
                    .writeTimeout(5, java.util.concurrent.TimeUnit.MINUTES)
                    .readTimeout(5, java.util.concurrent.TimeUnit.MINUTES)
                     .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://inshortsapi.vercel.app/")
                    .client(okHttpClient)
                     .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }catch (Exception e){
            Log.d("TAG"," exception retrofit : "+e);
        }

        return retrofit;
    }
    //check cnx
    public boolean isNetworkConnected(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
    //visibility
    public void VisibilityViews(List<NEWS.DATA> newsList, LinearLayout tv_nodata, RecyclerView recyclerView, ProgressBar spinner){
         if (newsList!=null&&newsList.size() > 0) {
            tv_nodata.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.INVISIBLE);
            tv_nodata.setVisibility(View.VISIBLE);
        }
        spinner.setVisibility(View.GONE);
    }
}
